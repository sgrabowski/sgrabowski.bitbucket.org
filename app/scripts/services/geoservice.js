'use strict';

/**
 * @ngdoc service
 * @name yeoApp.GeoService
 * @description
 * # GeoService
 * Factory in the yeoApp.
 */
angular.module('yeoApp')
.factory('GeoService',function ($q) {

    var map,
        geocoder,
        showLocation = function () {
          var d =  $q.defer();
            navigator.geolocation.getCurrentPosition(function(position){ 
              codeLatLng(position.coords.latitude, position.coords.longitude)
              .then(function (data) {
          d.resolve(data);
        });
      });
            return d.promise;
        },
        init = function (mp) {
            map = mp;
            geocoder = new google.maps.Geocoder();

        },
    codeLatLng = function (lat, lng) {
      var latlng = new google.maps.LatLng(lat, lng);
      var defer = $q.defer();

      geocoder.geocode({ 'latLng': latlng }, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          if (results[1]) {
            defer.resolve({
              address : results[0].formatted_address,
              latitude: lat,
              longitude:lng
            }
)
          }
          else {
            defer.reject("Nie znaleziono pasującego adresu.");
          }
        }
        else {
          defer.reject("Geolokalizacja nieudana z powodu: " + status);
        }
      });
      return defer.promise;
    },
    getLocation = function (address) {
        debugger;
      var defer =$q.defer();
      
      geocoder.geocode({ 'address': address }, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          var l = results[0].geometry.location;
          defer.resolve({address : results[0].formatted_address,
              latitude: l.lat(),
              longitude:l.lng()});
        }
        else defer.reject('Błąd dla adresu ' + address +': ' + status);
      });
      return defer.promise;
      
    }

    return {
        showLocation: showLocation,
        init:init,
        getLocation:getLocation
    }
});
