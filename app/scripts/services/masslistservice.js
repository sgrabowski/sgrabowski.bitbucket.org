'use strict'

/**
 * @ngdoc service
 * @name yeoApp.massListService
 * @description
 * # massListService
 * Factory in the yeoApp.
 */
angular.module('yeoApp')
   .factory('MassListService', function($q, $http) {
           var deferred = $q.defer();
           var toMinutes = function(s){
              var ts = s.split(":");
              return ts[0] * 60 + parseInt(ts[1], 10);
           }
           var getList = function(city) {
               $http({
                   method: 'GET',
                   url: city + '.json'
               }).
               success(function(data, status, headers, config) {
                   var idd = 1;
                   angular.forEach(data, function(d) {
                      d.latitude = d.lat;
                      d.longitude = d.long;
                      d.nd = (_.map(d.ndm,function(f){ 
                        return f.t+ ' '+(f.d||'');
                        }).join(','));
                      _.each(d.ndm,function(i){
                          i.ts = toMinutes(i.t);    
                      })
                      

                       //d.id = idd++;
                       // d.latitude = d.pos.lat;
                       // d.longitude = d.pos.long;
                       // delete d.pos;
                       // var hours = [];
                       // var tmp_hours = d.nd.split(",");

                       // angular.forEach(tmp_hours, function(f) {
                       //     var e = f.trim();
                       //     var hEnd = e.indexOf(" ");
                       //     if (hEnd > -1) {
                       //         var o = {
                       //             t: e.substr(0, hEnd),
                       //             d: e.substr(hEnd + 1)
                       //         }
                       //     } else {
                       //         var o = {
                       //             t: e
                       //         }
                       //     }
                       //     var ts = o.t.split(":");
                       //     o.ts = ts[0] * 60 + parseInt(ts[1], 10);
                       //     hours.push(o);
                       // });
                       // d.ndm = hours;

                   })
                   deferred.resolve(data);
               }).
               error(function(data, status, headers, config) {
                   deferred.reject(data);
               });
               return deferred.promise;
           }

           return {
               all: getList
           }

       })