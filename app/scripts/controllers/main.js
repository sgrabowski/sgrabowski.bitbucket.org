'use strict';

/**
 * @ngdoc function
 * @name yeoApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the yeoApp
 */
angular.module('yeoApp')
  .controller('MainCtrl', function ($scope) {
    $scope.todos = [];
  });
