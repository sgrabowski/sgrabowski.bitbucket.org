'use strict';

/**
 * @ngdoc function
 * @name yeoApp.controller:ListCtrl
 * @description
 * # ListCtrl
 * Controller of the yeoApp
 */
angular.module('yeoApp')
  .controller('ListCtrl',function($scope,$rootScope, $filter, MassListService, GeoService, $location) {

           var city = $location.path().substr(1);

           GeoService.init();

           $scope.churchMarkers = [];
           $scope.filteredChurchMarkers = [];
           MassListService.all(city).then(function(data) {
               angular.forEach(data, function(f) {
                   f.onClick = function() {
                       showDetails(f);
                   }
                   $scope.churchMarkers.push(f);
                   $scope.filteredChurchMarkers.push(f);
               });
              if($scope.from){
               $scope.filterItems('',$scope.from)
              }
           });
           var parseHour = function(h) {
               if (h) {
                   if (h.indexOf(":") > -1) {
                       var t = h.split(":");
                       var rest = parseInt(t[1], 10);
                       return t[0] * 60 + (rest || 0);
                   }
                   return h * 60;
               }
               return 0;
           }
           $scope.searchLocation = function() {
               GeoService.getLocation($scope.location).then(function(d) {
                   setCenter(d.latitude, d.longitude);
                   $scope.location = d.address;
               });

           }
           $scope.filterItems = function(v, from, to) {
               $scope.filteredChurchMarkers =
                   $filter('filter')($scope.churchMarkers, v);

               var tFrom = parseHour(from);
               var tTo = parseHour(to) || 1440;

               if (tFrom + tTo > 0) {
                   var ft = [];
                   angular.forEach($scope.filteredChurchMarkers, function(a) {
                       var has = false;
                       angular.forEach(a.ndm, function(ms) {
                           if (ms.ts >= tFrom && ms.ts <= tTo) {
                               ms.a = true;
                               has = true;
                           } else {
                               ms.a = false;
                           }
                       });
                       if (has) {
                           ft.push(a);
                       }
                   })
                   $scope.filteredChurchMarkers = ft;
               }

           };

           $scope.map = {
               center: {
                   latitude: 50.06465,
                   longitude: 19.944979999999997,
               },
               bounds: {},
               zoom: 12
           };
           $scope.range = {
               center: $scope.map.center,
               radius: 500000,
               stroke: {
                   color: '#08B21F',
                   weight: 2,
                   opacity: 1
               },
               fill: {
                   color: '#08B21F',
                   opacity: 0.05
               }
           };
           $scope.$watch('radius', function(n) {
               $scope.range.radius = n * 1000;
           });



           var setCenter = function(latitude, longitude) {
                   $scope.map.center.latitude = latitude,
                   $scope.map.center.longitude = longitude,
                   $scope.map.zoom = 12;
               },
               showDetails = function(o) {
                   $scope.activeDetails.o = o;
                   $scope.activeDetails.show = true;
               };
           $scope.detailsClose = function() {
               $scope.activeDetails.show = false;
               $scope.activeDetails.o = {
                   id: -1
               };
           }
           $scope.centerHere = function() {
               GeoService.showLocation().then(function(d) {
                   setCenter(d.latitude, d.longitude);
                   $scope.location = d.address;
               });
           }
           $scope.activeDetails = {
               o: {},
               show: false
           };


           //if sunday
var today = new Date();
if(today.getDay() == 2){
  var m = today.getMinutes();
  $scope.from = today.getHours()+":"+(m<10?"0":"")+m;
}



       });
