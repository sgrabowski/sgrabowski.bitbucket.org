'use strict';

/**
 * @ngdoc function
 * @name yeoApp.controller:CitiesCtrl
 * @description
 * # CitiesCtrl
 * Controller of the yeoApp
 */
angular.module('yeoApp')
  .controller('CitiesCtrl', function($scope, $rootScope, $location) {
           $scope.cities = [{
               name: '/krakow',
               title: "Kraków"
           }
           // , {
           //     name: '/wroclaw',
           //     title: "Wrocław"
           // }
           ];

           var city = $location.path().toLowerCase();
           if (!_.some($scope.cities, function(c) {
               c.name == city
           }))
               $location.path('/krakow');
           $scope.activeCity = city;

           $rootScope.$on('$locationChangeSuccess', function(event) {
               $scope.activeCity = $location.path().toLowerCase();
           })



       });