(function() {


    angular.module('gm').service('messService', function() {
        var m = [{
            "parafia": "Archikatedralna",
            "ulica": "Wawel 3",
            "www": "http://www.katedra-wawelska.pl",
            "id": 1,
            "lat": 50.054588,
            "long": 19.935152,
            "ndm": [{
                "t": "07:00",
                "d": "(j. łaciński)"
            }, {
                "t": "08:00"
            }, {
                "t": "09:00",
                "d": "(j. łaciński)"
            }, {
                "t": "10:00"
            }, {
                "t": "11:30"
            }, {
                "t": "16:30",
                "d": "(X-III)"
            }, {
                "t": "17:30",
                "d": "(IV-IX)"
            }]
        }, {
            "parafia": "Bazylika Mariacka (Wniebowzięcia NMP)",
            "ulica": "Rynek Główny",
            "www": "http://www.mariacki.com/",
            "id": 2,
            "lat": 50.061717,
            "long": 19.93895,
            "ndm": [{
                "t": "06:00"
            }, {
                "t": "07:00"
            }, {
                "t": "08:00"
            }, {
                "t": "09:00"
            }, {
                "t": "10:00"
            }, {
                "t": "11:15"
            }, {
                "t": "12:00"
            }, {
                "t": "13:00"
            }, {
                "t": "18:30"
            }]
        }, {
            "parafia": "bł. Anieli Salawy",
            "ulica": "al. Kijowska 29",
            "www": "http://www.anielasalawa.pl",
            "id": 3,
            "lat": 50.076735,
            "long": 19.916851,
            "ndm": [{
                "t": "07:30"
            }, {
                "t": "09:00"
            }, {
                "t": "10:30"
            }, {
                "t": "12:00"
            }, {
                "t": "18:00"
            }, {
                "t": "19:30"
            }]
        }, {
            "parafia": "bł. Jerzego Popiełuszki (Złocień)",
            "ulica": "ul. Agatowa 39",
            "www": "http://www.zlocien.diecezja-krakow.pl/",
            "id": 4,
            "lat": 50.022574,
            "long": 20.04932,
            "ndm": [{
                "t": "08:30"
            }, {
                "t": "11:30"
            }, {
                "t": "16:30"
            }]
        }, {
            "parafia": "Boskiego Zbawiciela",
            "ulica": "ul. św. Jacka 16",
            "www": "http://krakow.sds.pl",
            "id": 5,
            "lat": 50.039071,
            "long": 19.920412,
            "ndm": [{
                "t": "08:00"
            }, {
                "t": "09:30"
            }, {
                "t": "11:00",
                "d": "(dla rodziców i dzieci)"
            }, {
                "t": "13:00"
            }, {
                "t": "18:00"
            }]
        }, {
            "parafia": "Bożego Ciała",
            "ulica": "ul. Bożego Ciała 26",
            "www": "http://www.bozecialo.net",
            "id": 6,
            "lat": 50.049683,
            "long": 19.944538,
            "ndm": [{
                "t": "06:30"
            }, {
                "t": "08:00"
            }, {
                "t": "09:30"
            }, {
                "t": "11:00(z",
                "d": "udziałem dzieci)"
            }, {
                "t": "12:15"
            }, {
                "t": "16:00"
            }, {
                "t": "19:00"
            }]
        }, {
            "parafia": "Chrystusa Króla",
            "ulica": "ul. Zaskale 1",
            "www": "http://www.przegorzaly.pl",
            "id": 7,
            "lat": 50.047096,
            "long": 19.864763,
            "ndm": [{
                "t": "07:30"
            }, {
                "t": "09:00"
            }, {
                "t": "11:00",
                "d": "(dla dzieci)"
            }, {
                "t": "18:00"
            }]
        }, {
            "parafia": "Chrystusa Króla",
            "ulica": "al. 29 Listopada 195",
            "www": "http://www.gotyk.home.pl/",
            "id": 8,
            "lat": 50.100571,
            "long": 19.964425,
            "ndm": [{
                "t": "08:00"
            }, {
                "t": "09:30"
            }, {
                "t": "11:00"
            }, {
                "t": "12:30"
            }, {
                "t": "18:00"
            }]
        }, {
            "parafia": "Ducha Świętego",
            "ulica": "Podstolice 25, Wieliczka",
            "www": "http://www.podstolice.diecezja.krakow.pl",
            "id": 9,
            "lat": 49.9667,
            "long": 20.0167,
            "ndm": [{
                "t": "07:30"
            }, {
                "t": "09:00"
            }, {
                "t": "11:00"
            }, {
                "t": "16:00"
            }]
        }, {
            "parafia": "Jezusa Chrystusa Odkupiciela Człowieka",
            "ulica": "ul. Stelmachów 137",
            "www": "http://parafiaredemptor.diecezja.pl/",
            "id": 10,
            "lat": 50.09490272375515,
            "long": 19.904168844223022,
            "ndm": [{
                "t": "08:30"
            }, {
                "t": "10:00"
            }, {
                "t": "11:30"
            }, {
                "t": "19:00"
            }]
        }, {
            "parafia": "Kaplica Matki Bożej Częstochowskiej (Kostrze)",
            "ulica": "Kostrze",
            "www": "http://parafia.krakow.pl/user_pages.php?user_id=8&page_id=26",
            "id": 11,
            "lat": 50.030342,
            "long": 19.859019,
            "ndm": [{
                "t": "10:00"
            }, {
                "t": "16:00",
                "d": "(oprócz wakacji)"
            }]
        }, {
            "parafia": "Kaplica Szpitala Zakonu Bonifratrów",
            "ulica": "ul. Trynitarska 11",
            "www": "http://www.bonifratrzy.pl/index.php?option=18&action=articles_show&art_id=24&menu_id=63&page=10",
            "id": 12,
            "lat": 50.047474,
            "long": 19.945271,
            "ndm": [{
                "t": "0"
            }, {
                "t": "4375"
            }]
        }, {
            "parafia": "Kaplica św. Teresy",
            "ulica": "ul. Goryczkowa",
            "www": "",
            "id": 13,
            "lat": 50.015157,
            "long": 19.920314,
            "ndm": [{
                "t": "0"
            }, {
                "t": "333461921296296"
            }]
        }, {
            "parafia": "Klasztor pw. Opieki św. Józefa (Karmelitanki Bose)",
            "ulica": "ul. Łobzowska 40",
            "image": "location.jpg",
            "www": "http://www.karmel.pl/mniszki/lobzow/",
            "id": 14,
            "lat": 50.070941,
            "long": 19.930669,
            "ndm": [{
                "t": "0"
            }, {
                "t": "333438541666667"
            }]
        }];
        var i = 0;
        angular.forEach(m, function(a) {
            a.image = "location" + ((i++) % 4 + 1) + ".jpg";
        })

        return {
            get: m
        }

    });


})();
