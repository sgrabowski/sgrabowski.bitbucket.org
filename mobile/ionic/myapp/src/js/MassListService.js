(function() {
    angular.module('gm').factory('massList', massList);

    function massList($q, messService) {
        var deferred = $q.defer();

        return {
            all: getList
        }

        function toMinutes(s) {
            var ts = s.split(":");
            return ts[0] * 60 + parseInt(ts[1], 10);
        }

        function getList(city) {
            var data = messService.get;
            var idd = 1;
            angular.forEach(data, function(d) {
                d.latitude = d.lat;
                d.longitude = d.long;
                d.nd = (_.map(d.ndm, function(f) {
                    return f.t + ' ' + (f.d || '');
                }).join(','));
                _.each(d.ndm, function(i) {
                    i.ts = toMinutes(i.t);
                })

            })
            deferred.resolve(data);

            return deferred.promise;
        }


    }
    massList.$inject = ['$q', 'messService'];

})();
