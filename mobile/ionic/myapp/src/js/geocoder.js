(function() {
    angular.module('gm').service('geocoder', Geocoder);

    function Geocoder($q, $timeout) {
        var geocoder;
        return {
            searchByCoord: searchPosition,
            searchByName: searchLocation
        }

        function getAddress(defer,results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                var rr = [];
                var l = results[0].geometry.location;
                rr.push({
                    address: results[0].formatted_address,
                    latitude: l.lat(),
                    longitude: l.lng()
                });

                defer.resolve(rr);
            } else defer.reject('Błąd dla adresu ' + address + ': ' + status);
        }
        function search(criteria){
        	geocoder = geocoder || new google.maps.Geocoder();
            var defer = $q.defer();

            geocoder.geocode(criteria, function(results, status) {
                return getAddress(defer,results, status);
            });

            return defer.promise;
        }
        
        function searchPosition(lat, long) {
            var latlng = new google.maps.LatLng(lat, long);
            return search({
                'latLng': latlng
            })
        }

        function searchLocation(text) {
            return search({
                'address': text
            })
        }


    }

    Geocoder.$inject = ['$q', '$timeout']
})();
