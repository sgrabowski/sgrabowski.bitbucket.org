(function () {
    angular.module('gm').service('appCtx',appCtx);

    function appCtx() {
        var search = {
            from: null,
            to: null,
            radius: null,
            position: {
                latitude: null,
                longitude: null
            }
        }

        return {
            search: search
        }
    }
})();
