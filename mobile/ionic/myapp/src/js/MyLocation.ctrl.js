(function() {
    angular.module('gm').controller('MyLocationCtrl', MyLocationCtrl);
    function MyLocationCtrl($scope, geoLocation) {

        $scope.center = function() {
            GeoLocationService.currentLocation().then(function(location) {
                $scope.$emit('setPosition', location);
            });
        }
    }
    MyLocationCtrl.$inject = ['$scope', 'geoLocation'];
})();
