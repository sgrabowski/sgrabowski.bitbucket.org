angular.module('gm').controller('AppCtrl', 	
	['$scope','$ionicModal','$timeout','appCtx','$state',
   function($scope, $ionicModal,$timeout,AppCtx,$state){

  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/search.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  
  });
   $scope.search = AppCtx.search;

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);
    $state.go($state.current, {}, {reload: true});
    $scope.modal.hide();
  };



}]);