(function() {
    angular.module('gm').controller('MapCtrl', MapCtrl);

    function MapCtrl($scope, $stateParams, $ionicPopup, massList) {
        $scope.$on('setPosition',
            function(event, position) {
                $scope.map.center.latitude = position.latitude;
                $scope.map.center.longitude = position.longitude;
            });


        $scope.map = {

            center: {
                latitude: 50.06465,
                longitude: 19.944979999999997,
            },
            bounds: {},
            options: {
                mapTypeControl: true,
                mapTypeControlOptions: {
                    mapTypeIds: ['SATELLITE', 'ROADMAP', 'HYBRID', 'TERRAIN'],
                    style: 'HORIZONTAL_BAR',
                    position: 'BOTTOM_CENTER'
                },
                zoomControl: true,
                zoomControlOptions: {
                    style: 'LARGE',
                    position: 'RIGHT_BOTTOM'
                },
                // scaleControl: true,
                // streetViewControl: true,
                // streetViewControlOptions: {
                //     position: 'LEFT_TOP'
                // }
            },
            zoom: 12
        };





        var showDetails = function(scope) {
            return function(f) {
                scope.o = f;
                var alertPopup = $ionicPopup.alert({
                    title: f.parafia,
                    scope: scope,
                    template: '<div>Adres: {{o.ulica}}<br>Msze: {{o.nd}}</div>'
                });
                alertPopup.then(function(res) {
                    console.log('Thank you for not eating my delicious ice cream cone');
                });
            }
        }($scope.$new());

        $scope.filteredChurchMarkers = [];
        massList.all('')
            .then(function(data) {
                angular.forEach(data, function(f) {
                    //$scope.churchMarkers.push(f);
                    f.onClick = function() {
                        showDetails(f);
                    }

                    $scope.filteredChurchMarkers.push(f);
                });
            });
    }

MapCtrl.$inject = ['$scope', '$stateParams', '$ionicPopup', 'massList'];

})();
