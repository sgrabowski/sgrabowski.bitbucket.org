(function() {

    angular.module('gm').controller('LocationCtrl', LocationCtrl);


    function LocationCtrl($scope, Geocoder) {

        var autocomplete;
        var location = null;
        $scope.initialize = initialize;
        $scope.searchGPS = searchGPS;

        function initialize() {

            autocomplete = new google.maps.places.Autocomplete(document.getElementById('autocomplete'), {
                types: ['geocode']
            });

           // geolocate();
            google.maps.event.addListener(autocomplete, 'place_changed', function() {
                var place = this.getPlace();
                if(!place ) return;

                if(!place.geometry) {
                     Geocoder.searchByName(place.formatted_address)
                    .then(function(r) {
                        if (r[0]) {
                            $scope.search.position = r[0];
                        }

                    })
                    return;
                };

                var l = place.geometry.location;
                location = {
                    address: place.formatted_address,
                    latitude: l.lat(),
                    longitude: l.lng()
                };
                console.log(place)
                $scope.search.position = location;
            });
        }

        function geolocate() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function(position) {
                    var geolocation = new google.maps.LatLng(
                        position.coords.latitude, position.coords.longitude);
                    var circle = new google.maps.Circle({
                        center: geolocation,
                        radius: position.coords.accuracy
                    });
                    autocomplete.setBounds(circle.getBounds());
                });
            }
        }

        function searchGPS(txt) {
            navigator.geolocation.getCurrentPosition(function(position) {

                Geocoder.searchByCoord(position.coords.latitude, position.coords.longitude)
                    .then(function(r) {
                        if (r[0]) {
                            location = r[0];
                            document.getElementById('autocomplete').value = location.address;
                            $scope.search.position = location;
                        }

                    })

            });


        }

    }
    LocationCtrl.$inject = ['$scope', 'geocoder'];

})();
