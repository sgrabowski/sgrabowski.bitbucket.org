(function () {
    
angular.module('gm').controller('ChurchDetailsCtrl',ChurchDetailsCtrl);

function ChurchDetailsCtrl($scope, $stateParams, messService, PreviousState) {
        $scope.backURL = PreviousState.URL;
        angular.forEach(messService.get, function(f) {
            if (f.id == $stateParams.id) {
                $scope.church = f;
            }
            return;
        })
    }

ChurchDetailsCtrl.$inject = ['$scope', '$stateParams', 'messService', 'PreviousState'];

})();