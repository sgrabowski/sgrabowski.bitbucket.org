angular.module('gm', ['ionic','starter.controllers','timepicker'
 ,'uiGmapgoogle-maps','ngCordova'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

  .state('app', {
    url: "/app",
    abstract: true,
    templateUrl: "templates/menu.html",
    controller: 'AppCtrl'
  })

  .state('app.timeList', {
    url: "/time",
    views: {
      'menuContent': {   
        controller:'ChurchListCtrl as vm',
        templateUrl: "templates/time.html"
      }
    }
  })

  .state('app.churchList', {
    url: "/churches",
    views: {
      'menuContent': {
        controller:'ChurchListCtrl as vm',
        templateUrl: "templates/churchList.html"
      }
    }
  })
   .state('details', {
    url: "/details/{id}",
    resolve: {
          PreviousState: [
              "$state",
              function ($state) {
                  var currentStateData = {
                      Name: $state.current.name,
                      Params: $state.params,
                      URL: $state.href($state.current.name, $state.params)
                  };
                  return currentStateData;
              }
          ]
            },
    controller:'ChurchDetailsCtrl',
    templateUrl: "templates/details.html"
    
  })
       .state('app.map', {
      url: "/playlists",
      views: {
        'menuContent': {
          templateUrl: "templates/map.html",
          controller: 'MapCtrl'
        }
      }
    })
    .state('app.playlists', {
      url: "/playlists",
      views: {
        'menuContent': {
          templateUrl: "templates/playlists.html",
          controller: 'PlaylistsCtrl'
        }
      }
    })

  .state('app.single', {
    url: "/playlists/:playlistId",
    views: {
      'menuContent': {
        templateUrl: "templates/playlist.html",
        controller: 'PlaylistCtrl'
      }
    }
  });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/playlists');
})
.config(function(uiGmapGoogleMapApiProvider) {
    uiGmapGoogleMapApiProvider.configure({
        //    key: 'your api key',
        v: '3.17',
        libraries: 'weather,geometry,visualization'
    });
})
