(function() {
        angular.module('gm').factory('geoLocation', geoLocation);

        function geoLocation($cordovaGeolocation) {
            return {
                currentLocation: cl
            }

            function cl() {

                var posOptions = {
                    timeout: 10000,
                    enableHighAccuracy: false
                };
                return $cordovaGeolocation
                    .getCurrentPosition(posOptions)
                    .then(function(position) {
                        return position.coords;
                    }, function(err) {
                        // error
                    });

            }
        }
    geoLocation.$inject = ['$cordovaGeolocation'];
})();
