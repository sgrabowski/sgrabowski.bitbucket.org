angular.module('gm', ['ionic','starter.controllers','timepicker'
 ,'uiGmapgoogle-maps','ngCordova'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

  .state('app', {
    url: "/app",
    abstract: true,
    templateUrl: "templates/menu.html",
    controller: 'AppCtrl'
  })

  .state('app.timeList', {
    url: "/time",
    views: {
      'menuContent': {   
        controller:'ChurchListCtrl as vm',
        templateUrl: "templates/time.html"
      }
    }
  })

  .state('app.churchList', {
    url: "/churches",
    views: {
      'menuContent': {
        controller:'ChurchListCtrl as vm',
        templateUrl: "templates/churchList.html"
      }
    }
  })
   .state('details', {
    url: "/details/{id}",
    resolve: {
          PreviousState: [
              "$state",
              function ($state) {
                  var currentStateData = {
                      Name: $state.current.name,
                      Params: $state.params,
                      URL: $state.href($state.current.name, $state.params)
                  };
                  return currentStateData;
              }
          ]
            },
    controller:'ChurchDetailsCtrl',
    templateUrl: "templates/details.html"
    
  })
       .state('app.map', {
      url: "/playlists",
      views: {
        'menuContent': {
          templateUrl: "templates/map.html",
          controller: 'MapCtrl'
        }
      }
    })
    .state('app.playlists', {
      url: "/playlists",
      views: {
        'menuContent': {
          templateUrl: "templates/playlists.html",
          controller: 'PlaylistsCtrl'
        }
      }
    })

  .state('app.single', {
    url: "/playlists/:playlistId",
    views: {
      'menuContent': {
        templateUrl: "templates/playlist.html",
        controller: 'PlaylistCtrl'
      }
    }
  });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/playlists');
})
.config(function(uiGmapGoogleMapApiProvider) {
    uiGmapGoogleMapApiProvider.configure({
        //    key: 'your api key',
        v: '3.17',
        libraries: 'weather,geometry,visualization'
    });
})

angular.module('gm').controller('AppCtrl', 	
	['$scope','$ionicModal','$timeout','appCtx','$state',
   function($scope, $ionicModal,$timeout,AppCtx,$state){

  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/search.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  
  });
   $scope.search = AppCtx.search;

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);
    $state.go($state.current, {}, {reload: true});
    $scope.modal.hide();
  };



}]);
(function () {
    
angular.module('gm').controller('ChurchDetailsCtrl',ChurchDetailsCtrl);

function ChurchDetailsCtrl($scope, $stateParams, messService, PreviousState) {
        $scope.backURL = PreviousState.URL;
        angular.forEach(messService.get, function(f) {
            if (f.id == $stateParams.id) {
                $scope.church = f;
            }
            return;
        })
    }

ChurchDetailsCtrl.$inject = ['$scope', '$stateParams', 'messService', 'PreviousState'];

})();
(function() {

    angular.module('gm').controller('ChurchListCtrl', ChurchListCtrl);

    function ChurchListCtrl($scope, $stateParams, messService) {
        this.items = messService.get;
    }
    ChurchListCtrl.$inject = ['$scope', '$stateParams', 'messService'];

})();

(function() {

    angular.module('gm').controller('LocationCtrl', LocationCtrl);


    function LocationCtrl($scope, Geocoder) {

        var autocomplete;
        var location = null;
        $scope.initialize = initialize;
        $scope.searchGPS = searchGPS;

        function initialize() {

            autocomplete = new google.maps.places.Autocomplete(document.getElementById('autocomplete'), {
                types: ['geocode']
            });

           // geolocate();
            google.maps.event.addListener(autocomplete, 'place_changed', function() {
                var place = this.getPlace();
                if(!place ) return;

                if(!place.geometry) {
                     Geocoder.searchByName(place.formatted_address)
                    .then(function(r) {
                        if (r[0]) {
                            $scope.search.position = r[0];
                        }

                    })
                    return;
                };

                var l = place.geometry.location;
                location = {
                    address: place.formatted_address,
                    latitude: l.lat(),
                    longitude: l.lng()
                };
                console.log(place)
                $scope.search.position = location;
            });
        }

        function geolocate() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function(position) {
                    var geolocation = new google.maps.LatLng(
                        position.coords.latitude, position.coords.longitude);
                    var circle = new google.maps.Circle({
                        center: geolocation,
                        radius: position.coords.accuracy
                    });
                    autocomplete.setBounds(circle.getBounds());
                });
            }
        }

        function searchGPS(txt) {
            navigator.geolocation.getCurrentPosition(function(position) {

                Geocoder.searchByCoord(position.coords.latitude, position.coords.longitude)
                    .then(function(r) {
                        if (r[0]) {
                            location = r[0];
                            document.getElementById('autocomplete').value = location.address;
                            $scope.search.position = location;
                        }

                    })

            });


        }

    }
    LocationCtrl.$inject = ['$scope', 'geocoder'];

})();

(function() {
    angular.module('gm').controller('MapCtrl', MapCtrl);

    function MapCtrl($scope, $stateParams, $ionicPopup, massList) {
        $scope.$on('setPosition',
            function(event, position) {
                $scope.map.center.latitude = position.latitude;
                $scope.map.center.longitude = position.longitude;
            });


        $scope.map = {

            center: {
                latitude: 50.06465,
                longitude: 19.944979999999997,
            },
            bounds: {},
            options: {
                mapTypeControl: true,
                mapTypeControlOptions: {
                    mapTypeIds: ['SATELLITE', 'ROADMAP', 'HYBRID', 'TERRAIN'],
                    style: 'HORIZONTAL_BAR',
                    position: 'BOTTOM_CENTER'
                },
                zoomControl: true,
                zoomControlOptions: {
                    style: 'LARGE',
                    position: 'RIGHT_BOTTOM'
                },
                // scaleControl: true,
                // streetViewControl: true,
                // streetViewControlOptions: {
                //     position: 'LEFT_TOP'
                // }
            },
            zoom: 12
        };





        var showDetails = function(scope) {
            return function(f) {
                scope.o = f;
                var alertPopup = $ionicPopup.alert({
                    title: f.parafia,
                    scope: scope,
                    template: '<div>Adres: {{o.ulica}}<br>Msze: {{o.nd}}</div>'
                });
                alertPopup.then(function(res) {
                    console.log('Thank you for not eating my delicious ice cream cone');
                });
            }
        }($scope.$new());

        $scope.filteredChurchMarkers = [];
        massList.all('')
            .then(function(data) {
                angular.forEach(data, function(f) {
                    //$scope.churchMarkers.push(f);
                    f.onClick = function() {
                        showDetails(f);
                    }

                    $scope.filteredChurchMarkers.push(f);
                });
            });
    }

MapCtrl.$inject = ['$scope', '$stateParams', '$ionicPopup', 'massList'];

})();

(function() {
    angular.module('gm').factory('massList', massList);

    function massList($q, messService) {
        var deferred = $q.defer();

        return {
            all: getList
        }

        function toMinutes(s) {
            var ts = s.split(":");
            return ts[0] * 60 + parseInt(ts[1], 10);
        }

        function getList(city) {
            var data = messService.get;
            var idd = 1;
            angular.forEach(data, function(d) {
                d.latitude = d.lat;
                d.longitude = d.long;
                d.nd = (_.map(d.ndm, function(f) {
                    return f.t + ' ' + (f.d || '');
                }).join(','));
                _.each(d.ndm, function(i) {
                    i.ts = toMinutes(i.t);
                })

            })
            deferred.resolve(data);

            return deferred.promise;
        }


    }
    massList.$inject = ['$q', 'messService'];

})();

(function() {
    angular.module('gm').controller('MyLocationCtrl', MyLocationCtrl);
    function MyLocationCtrl($scope, geoLocation) {

        $scope.center = function() {
            GeoLocationService.currentLocation().then(function(location) {
                $scope.$emit('setPosition', location);
            });
        }
    }
    MyLocationCtrl.$inject = ['$scope', 'geoLocation'];
})();

(function () {
    angular.module('gm').service('appCtx',appCtx);

    function appCtx() {
        var search = {
            from: null,
            to: null,
            radius: null,
            position: {
                latitude: null,
                longitude: null
            }
        }

        return {
            search: search
        }
    }
})();

angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {
  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    // $timeout(function() {
    //   $scope.closeLogin();
    // }, 1000);
  };





  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/location.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.locationPopup = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLocation = function() {
    $scope.locationPopup.hide();
  };


})

.controller('PlaylistsCtrl', function($scope) {
  $scope.playlists = [
    { title: 'Reggae', id: 1 },
    { title: 'Chill', id: 2 },
    { title: 'Dubstep', id: 3 },
    { title: 'Indie', id: 4 },
    { title: 'Rap', id: 5 },
    { title: 'Cowbell', id: 6 }
  ];
})

.controller('PlaylistCtrl', function($scope, $stateParams) {

$scope.map = { center: { latitude: 45, longitude: -73 }, zoom: 8 };
});







(function() {
        angular.module('gm').factory('geoLocation', geoLocation);

        function geoLocation($cordovaGeolocation) {
            return {
                currentLocation: cl
            }

            function cl() {

                var posOptions = {
                    timeout: 10000,
                    enableHighAccuracy: false
                };
                return $cordovaGeolocation
                    .getCurrentPosition(posOptions)
                    .then(function(position) {
                        return position.coords;
                    }, function(err) {
                        // error
                    });

            }
        }
    geoLocation.$inject = ['$cordovaGeolocation'];
})();

(function() {
    angular.module('gm').service('geocoder', Geocoder);

    function Geocoder($q, $timeout) {
        var geocoder;
        return {
            searchByCoord: searchPosition,
            searchByName: searchLocation
        }

        function getAddress(defer,results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                var rr = [];
                var l = results[0].geometry.location;
                rr.push({
                    address: results[0].formatted_address,
                    latitude: l.lat(),
                    longitude: l.lng()
                });

                defer.resolve(rr);
            } else defer.reject('Błąd dla adresu ' + address + ': ' + status);
        }
        function search(criteria){
        	geocoder = geocoder || new google.maps.Geocoder();
            var defer = $q.defer();

            geocoder.geocode(criteria, function(results, status) {
                return getAddress(defer,results, status);
            });

            return defer.promise;
        }
        
        function searchPosition(lat, long) {
            var latlng = new google.maps.LatLng(lat, long);
            return search({
                'latLng': latlng
            })
        }

        function searchLocation(text) {
            return search({
                'address': text
            })
        }


    }

    Geocoder.$inject = ['$q', '$timeout']
})();

(function() {


    angular.module('gm').service('messService', function() {
        var m = [{
            "parafia": "Archikatedralna",
            "ulica": "Wawel 3",
            "www": "http://www.katedra-wawelska.pl",
            "id": 1,
            "lat": 50.054588,
            "long": 19.935152,
            "ndm": [{
                "t": "07:00",
                "d": "(j. łaciński)"
            }, {
                "t": "08:00"
            }, {
                "t": "09:00",
                "d": "(j. łaciński)"
            }, {
                "t": "10:00"
            }, {
                "t": "11:30"
            }, {
                "t": "16:30",
                "d": "(X-III)"
            }, {
                "t": "17:30",
                "d": "(IV-IX)"
            }]
        }, {
            "parafia": "Bazylika Mariacka (Wniebowzięcia NMP)",
            "ulica": "Rynek Główny",
            "www": "http://www.mariacki.com/",
            "id": 2,
            "lat": 50.061717,
            "long": 19.93895,
            "ndm": [{
                "t": "06:00"
            }, {
                "t": "07:00"
            }, {
                "t": "08:00"
            }, {
                "t": "09:00"
            }, {
                "t": "10:00"
            }, {
                "t": "11:15"
            }, {
                "t": "12:00"
            }, {
                "t": "13:00"
            }, {
                "t": "18:30"
            }]
        }, {
            "parafia": "bł. Anieli Salawy",
            "ulica": "al. Kijowska 29",
            "www": "http://www.anielasalawa.pl",
            "id": 3,
            "lat": 50.076735,
            "long": 19.916851,
            "ndm": [{
                "t": "07:30"
            }, {
                "t": "09:00"
            }, {
                "t": "10:30"
            }, {
                "t": "12:00"
            }, {
                "t": "18:00"
            }, {
                "t": "19:30"
            }]
        }, {
            "parafia": "bł. Jerzego Popiełuszki (Złocień)",
            "ulica": "ul. Agatowa 39",
            "www": "http://www.zlocien.diecezja-krakow.pl/",
            "id": 4,
            "lat": 50.022574,
            "long": 20.04932,
            "ndm": [{
                "t": "08:30"
            }, {
                "t": "11:30"
            }, {
                "t": "16:30"
            }]
        }, {
            "parafia": "Boskiego Zbawiciela",
            "ulica": "ul. św. Jacka 16",
            "www": "http://krakow.sds.pl",
            "id": 5,
            "lat": 50.039071,
            "long": 19.920412,
            "ndm": [{
                "t": "08:00"
            }, {
                "t": "09:30"
            }, {
                "t": "11:00",
                "d": "(dla rodziców i dzieci)"
            }, {
                "t": "13:00"
            }, {
                "t": "18:00"
            }]
        }, {
            "parafia": "Bożego Ciała",
            "ulica": "ul. Bożego Ciała 26",
            "www": "http://www.bozecialo.net",
            "id": 6,
            "lat": 50.049683,
            "long": 19.944538,
            "ndm": [{
                "t": "06:30"
            }, {
                "t": "08:00"
            }, {
                "t": "09:30"
            }, {
                "t": "11:00(z",
                "d": "udziałem dzieci)"
            }, {
                "t": "12:15"
            }, {
                "t": "16:00"
            }, {
                "t": "19:00"
            }]
        }, {
            "parafia": "Chrystusa Króla",
            "ulica": "ul. Zaskale 1",
            "www": "http://www.przegorzaly.pl",
            "id": 7,
            "lat": 50.047096,
            "long": 19.864763,
            "ndm": [{
                "t": "07:30"
            }, {
                "t": "09:00"
            }, {
                "t": "11:00",
                "d": "(dla dzieci)"
            }, {
                "t": "18:00"
            }]
        }, {
            "parafia": "Chrystusa Króla",
            "ulica": "al. 29 Listopada 195",
            "www": "http://www.gotyk.home.pl/",
            "id": 8,
            "lat": 50.100571,
            "long": 19.964425,
            "ndm": [{
                "t": "08:00"
            }, {
                "t": "09:30"
            }, {
                "t": "11:00"
            }, {
                "t": "12:30"
            }, {
                "t": "18:00"
            }]
        }, {
            "parafia": "Ducha Świętego",
            "ulica": "Podstolice 25, Wieliczka",
            "www": "http://www.podstolice.diecezja.krakow.pl",
            "id": 9,
            "lat": 49.9667,
            "long": 20.0167,
            "ndm": [{
                "t": "07:30"
            }, {
                "t": "09:00"
            }, {
                "t": "11:00"
            }, {
                "t": "16:00"
            }]
        }, {
            "parafia": "Jezusa Chrystusa Odkupiciela Człowieka",
            "ulica": "ul. Stelmachów 137",
            "www": "http://parafiaredemptor.diecezja.pl/",
            "id": 10,
            "lat": 50.09490272375515,
            "long": 19.904168844223022,
            "ndm": [{
                "t": "08:30"
            }, {
                "t": "10:00"
            }, {
                "t": "11:30"
            }, {
                "t": "19:00"
            }]
        }, {
            "parafia": "Kaplica Matki Bożej Częstochowskiej (Kostrze)",
            "ulica": "Kostrze",
            "www": "http://parafia.krakow.pl/user_pages.php?user_id=8&page_id=26",
            "id": 11,
            "lat": 50.030342,
            "long": 19.859019,
            "ndm": [{
                "t": "10:00"
            }, {
                "t": "16:00",
                "d": "(oprócz wakacji)"
            }]
        }, {
            "parafia": "Kaplica Szpitala Zakonu Bonifratrów",
            "ulica": "ul. Trynitarska 11",
            "www": "http://www.bonifratrzy.pl/index.php?option=18&action=articles_show&art_id=24&menu_id=63&page=10",
            "id": 12,
            "lat": 50.047474,
            "long": 19.945271,
            "ndm": [{
                "t": "0"
            }, {
                "t": "4375"
            }]
        }, {
            "parafia": "Kaplica św. Teresy",
            "ulica": "ul. Goryczkowa",
            "www": "",
            "id": 13,
            "lat": 50.015157,
            "long": 19.920314,
            "ndm": [{
                "t": "0"
            }, {
                "t": "333461921296296"
            }]
        }, {
            "parafia": "Klasztor pw. Opieki św. Józefa (Karmelitanki Bose)",
            "ulica": "ul. Łobzowska 40",
            "image": "location.jpg",
            "www": "http://www.karmel.pl/mniszki/lobzow/",
            "id": 14,
            "lat": 50.070941,
            "long": 19.930669,
            "ndm": [{
                "t": "0"
            }, {
                "t": "333438541666667"
            }]
        }];
        var i = 0;
        angular.forEach(m, function(a) {
            a.image = "location" + ((i++) % 4 + 1) + ".jpg";
        })

        return {
            get: m
        }

    });


})();

angular.module("timepicker",[])
.directive('standardTimeMeridian', function () {
        return {
            restrict: 'AE',
            replace: true,
            scope: {
                etime: '=etime'
            },
            template: "<strong>{{stime}}</strong>",
            link: function (scope, elem, attrs) {

                scope.stime = epochParser(scope.etime, 'time');

                function prependZero(param) {
                    if (String(param).length < 2) {
                        return "0" + String(param);
                    }
                    return param;
                }

                function epochParser(val, opType) {
                    if (val === null) {
                        return "00:00";
                    } else {
                        var meridian = ['AM', 'PM'];

                        if (opType === 'time') {
                            var hours = parseInt(val / 3600);
                            var minutes = (val / 60) % 60;
                            var hoursRes = hours > 12 ? (hours - 12) : hours;

                            var currentMeridian = meridian[parseInt(hours / 12)];

                            return (prependZero(hoursRes) + ":" + prependZero(minutes) + " " + currentMeridian);
                        }
                    }
                }

                scope.$watch('etime', function (newValue, oldValue) {
                    scope.stime = epochParser(scope.etime, 'time');
                });

            }
        };
    })

    .directive('standardTimeNoMeridian', function () {
        return {
            restrict: 'AE',
            replace: true,
            scope: {
                etime: '=etime'
            },
            template: "<strong>{{stime}}</strong>",
            link: function (scope, elem, attrs) {

                scope.stime = epochParser(scope.etime, 'time');

                function prependZero(param) {
                    if (String(param).length < 2) {
                        return "0" + String(param);
                    }
                    return param;
                }

                function epochParser(val, opType) {
                    if (val === null) {
                        return "00:00";
                    } else {
                        if (opType === 'time') {
                            var hours = parseInt(val / 3600);
                            var minutes = (val / 60) % 60;

                            return (prependZero(hours) + ":" + prependZero(minutes));
                        }
                    }
                }

                scope.$watch('etime', function (newValue, oldValue) {
                    scope.stime = epochParser(scope.etime, 'time');
                });

            }
        };
    })

    .directive('ionicTimePicker', function ($ionicPopup) {
        return {
            restrict: 'AE',
            replace: true,
            scope: {
                etime: '=etime',
                format: '=format',
                step: '=step'
            },
            link: function (scope, element, attrs) {

                element.on("click", function () {

                    var obj = {epochTime: scope.etime, step: scope.step, format: scope.format};

                    scope.time = { hours: 0, minutes: 0, meridian: "" };

                    var objDate = new Date(obj.epochTime * 1000);       // Epoch time in milliseconds.

                    scope.increaseHours = function () {
                        if (obj.format == 12) {
                            if (scope.time.hours != 12) {
                                scope.time.hours += 1;
                            } else {
                                scope.time.hours = 1;
                            }
                        }
                        if (obj.format == 24) {
                            if (scope.time.hours != 23) {
                                scope.time.hours += 1;
                            } else {
                                scope.time.hours = 0;
                            }
                        }
                    };

                    scope.decreaseHours = function () {
                        if (obj.format == 12) {
                            if (scope.time.hours > 1) {
                                scope.time.hours -= 1;
                            } else {
                                scope.time.hours = 12;
                            }
                        }
                        if (obj.format == 24) {
                            if (scope.time.hours > 0) {
                                scope.time.hours -= 1;
                            } else {
                                scope.time.hours = 23;
                            }
                        }
                    };

                    scope.increaseMinutes = function () {
                        if (scope.time.minutes != (60 - obj.step)) {
                            scope.time.minutes += obj.step;
                        } else {
                            scope.time.minutes = 0;
                        }
                    };

                    scope.decreaseMinutes = function () {
                        if (scope.time.minutes != 0) {
                            scope.time.minutes -= obj.step;
                        } else {
                            scope.time.minutes = 60 - obj.step;
                        }
                    };

                    if (obj.format == 12) {

                        scope.time.meridian = (objDate.getUTCHours() >= 12) ? "PM" : "AM";
                        scope.time.hours = (objDate.getUTCHours() > 12) ? ((objDate.getUTCHours() - 12)) : (objDate.getUTCHours());
                        scope.time.minutes = (objDate.getUTCMinutes());

                        if (scope.time.hours == 0 && scope.time.meridian == "AM") {
                            scope.time.hours = 12;
                        }

                        scope.changeMeridian = function () {
                            scope.time.meridian = (scope.time.meridian === "AM") ? "PM" : "AM";
                        };

                        $ionicPopup.show({
                            templateUrl: '../templates/time-picker-12-hour.html',
                            title: '<strong>12-Hour Format</strong>',
                            subTitle: '',
                            scope: scope,
                            buttons: [
                                { text: 'Cancel' },
                                {
                                    text: 'Set',
                                    type: 'button-positive',
                                    onTap: function (e) {

                                        scope.loadingContent = true;

                                        var totalSec = 0;

                                        if (scope.time.hours != 12) {
                                            totalSec = (scope.time.hours * 60 * 60) + (scope.time.minutes * 60);
                                        } else {
                                            totalSec = scope.time.minutes * 60;
                                        }

                                        if (scope.time.meridian === "AM") {
                                            totalSec += 0;
                                        } else if (scope.time.meridian === "PM") {
                                            totalSec += 43200;
                                        }
                                        scope.etime = totalSec;
                                    }
                                }
                            ]
                        })

                    }

                    if (obj.format == 24) {

                        scope.time.hours = (objDate.getUTCHours());
                        scope.time.minutes = (objDate.getUTCMinutes());

                        $ionicPopup.show({
                            templateUrl: '../templates/time-picker-24-hour.html',
                            title: '<strong>24-Hour Format</strong>',
                            subTitle: '',
                            scope: scope,
                            buttons: [
                                { text: 'Cancel' },
                                {
                                    text: 'Set',
                                    type: 'button-positive',
                                    onTap: function (e) {

                                        scope.loadingContent = true;

                                        var totalSec = 0;

                                        if (scope.time.hours != 24) {
                                            totalSec = (scope.time.hours * 60 * 60) + (scope.time.minutes * 60);
                                        } else {
                                            totalSec = scope.time.minutes * 60;
                                        }
                                        scope.etime = totalSec;
                                    }
                                }
                            ]
                        })

                    }

                });

            }
        };
    });