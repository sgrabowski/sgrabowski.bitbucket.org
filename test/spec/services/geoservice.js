'use strict';

describe('Service: GeoService', function () {

  // load the service's module
  beforeEach(module('yeoApp'));

  // instantiate service
  var GeoService;
  beforeEach(inject(function (_GeoService_) {
    GeoService = _GeoService_;
  }));

  it('should do something', function () {
    expect(!!GeoService).toBe(true);
  });

});
