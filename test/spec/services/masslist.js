'use strict';

describe('Service: massList', function () {

  // load the service's module
  beforeEach(module('yeoApp'));

  // instantiate service
  var massList;
  beforeEach(inject(function (_massList_) {
    massList = _massList_;
  }));

  it('should do something', function () {
    expect(!!massList).toBe(true);
  });

});
