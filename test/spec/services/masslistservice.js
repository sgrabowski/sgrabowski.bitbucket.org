'use strict';

describe('Service: massListService', function () {

  // load the service's module
  beforeEach(module('yeoApp'));

  // instantiate service
  var massListService;
  beforeEach(inject(function (_massListService_) {
    massListService = _massListService_;
  }));

  it('should do something', function () {
    expect(!!massListService).toBe(true);
  });

});
